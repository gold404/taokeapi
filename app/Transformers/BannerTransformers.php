<?php

namespace App\Transformers;

use App\Models\Banner;
use League\Fractal\TransformerAbstract;

class BannerTransformers extends TransformerAbstract
{
    public function transform(Banner $banner){
        return [
            'id' =>  $banner->id,
            'title' => $banner->title,
            'image' => $banner -> image,
            'link' => $banner -> link,
            'sort' => $banner->sort,
            'created_at' => $banner->created_at,
            'updated_at' => $banner->updated_at
        ];
    }
}
