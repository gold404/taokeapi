<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;

class RegisterRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:16',
            'email' => 'required|email|unique:users',//邮箱在user表中唯一
            'password' =>'required|min:6|max:32|confirmed'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '请填写昵称',
            'name.max' => '昵称不能超过16字符'
        ];
    }
}
