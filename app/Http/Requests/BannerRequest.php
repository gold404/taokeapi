<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'image' => 'required'
        ];
    }

    public function messages()
    {
        return [
          'title.required' => '标题为必填项',
          'image.require' => '图片不能为空'
        ];
    }
}
