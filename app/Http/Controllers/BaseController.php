<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    use Helpers;

    /**
     * 淘宝联盟SDK
     * @return \TopClient
     */
    public function taobao_c(){
        $c = new \TopClient();
        $c->appkey = env('taobao_appkey');
        $c->secretKey=env('taobao_secretKey');
        $c->format='json';
        return $c;
    }
}
