<?php

namespace App\Http\Controllers\Taobao;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\ResourceAbstract;

class IndexController extends BaseController
{
    /**
     * 每日推荐/精品推荐/今日爆款（综合类目）物料id：30443
     * @param Request $request
     * @return false|string
     */
    public function recommend(Request $request){
        $data=$this->fromMaterial(30443,1,5);
        return $data;
        return $this->response->array($data);
//        return $this->fromMaterial(30443,$request->get('page'),$request->get('pageSize'));
    }

    /**
     * 商品详情
     * @param Request $request 商品id
     * @return false|string
     */
    public function detail(Request $request){
        $req=new \TbkItemInfoGetRequest();
        $req->setNumIids($request->get('goods_id'));
        $req->setPlatform("1");//链接形式：1：PC，2：无线，默认：１
        $req->setIp($request->getClientIp());//计算邮费
        $res = $this->taobao_c()->execute($req);
        if (empty($res->results)){
            return json_encode($res);
        }
        return json_encode($res->results->n_tbk_item);
    }

    /**
     * 对接淘宝联盟物料精选
     * 淘宝物料id查看链接：https://market.m.taobao.com/app/qn/toutiao-new/index-pc.html#/detail/10628875?_k=gpov9a
     * @param $material_id 淘宝联盟物料id
     * @param int $page
     * @param int $pageSize
     * @return false|string
     */
    public function fromMaterial($material_id,$page=1,$pageSize=20,$goods_id=''){
        $keys=['title','sub_title','small_images','nick'];
        $req = new \TbkDgOptimusMaterialRequest();
        $req->setAdzoneId(env('taobao_adzone_id'));
        $req->setPageSize($pageSize);
        $req->setPageNo($page);
        $req->setMaterialId($material_id);
        $goods_id ? $req->setItemId($goods_id) : false;
        $res = $this->taobao_c()->execute($req);
        if (empty($res->result_list)){
            return json_encode($res);
        }
//        return $res->result_list->map_data;
        $fractal = new Manager();
        $resource=new Collection($res->result_list->map_data,function ($data){
            return [
                'goods_id'=>$data->item_id,
                'category_id'=>$data->category_id,
                'title'=>$data->title,
                'short_title'=>$data->short_title,
                'sub_title' =>$data->sub_title,
                'description'=>$data->item_description,
                'avatar'=>$data->pict_url,
                'shop_title'=>$data->nick,
                'commission_rate'=>$data->commission_rate,//佣金比率%
                'coupon_amount'=>$data->coupon_amount,//优惠券
                'user_type' =>$data->user_type, //0表示淘宝，1表示天猫，3表示特价版
                'volume' =>$data->volume, //月销
                'zk_final_price'=>$data->zk_final_price,//折扣价
                'coupon_click_url'=>$data->coupon_click_url ?? '',
            ];
        });
        return $fractal->createData($resource);
    }

    /**
     * 生成短链接淘口令
     * @return false|string
     */
    public function getKouLing(){
//        长连接转短链接
        $req = new \TbkSpreadGetRequest;
        $requests = new \TbkSpreadRequest;
//        $requests->url="https://detail.tmall.com/item.htm?id=635588011068";
        $requests->url='uland.taobao.com/coupon/edetail?e=hxB%2Fvjbz6IoNfLV8niU3R5TgU2jJNKOfNNtsjZw%2F%2FoJbL%2FH2ftooabeN2aoazoWNy9vMwNNQ%2BMdKVZ5ZksddLmuFqp8TFaHMNg4Gqf8CT4DalCKtwD3CzAbmoRMamOw7mMHpNfYdHdDd2pfftp2hrP%2FmQ0Wi8xq1t7zw03ux55qB86TENrnkD9PteZljJxxlAhq7rbWA7zY%3D&&app_pvid=59590_33.50.183.2_778_1625652812766&ptl=floorId:30443;app_pvid:59590_33.50.183.2_778_1625652812766;tpp_pvid:52ba16f9-4930-4682-99a8-d6b86b1a3d18&union_lens=lensId%3AMAPI%401625652812%4052ba16f9-4930-4682-99a8-d6b86b1a3d18_558904397219%401';
        $req->setRequests(json_encode($requests));
        $resp = $this->taobao_c()->execute($req);
//        dd($resp);
//        return $resp->results->tbk_spread[0]->content;
        $req=new \TbkTpwdCreateRequest();
        $req->setUrl($resp->results->tbk_spread[0]->content);
        $res = $this->taobao_c()->execute($req);
        return json_encode($res->data->model);
    }

    /**
     * 相似推荐（使用需要先签署阿里的之智能导购协议）
     * @param Request $request
     * @return false|string
     */
    public function similar(Request $request){
        return $this->fromMaterial('13256',1,$request->get('pageSize') ?? 6,$request->get('goods_id'));
    }

    /**
     * 根据分类的material_id返回商品类表
     * @param Request $request
     * @return false|string
     */
    public function categoryList(Request $request){
        return $this->fromMaterial($request->get('material_id'),$request->get('page'),$request->get('pageSize'));
    }

    /**
     * 淘宝联盟分类小标题
     * @return \Dingo\Api\Http\Response
     */
    public function category(){
        $category=[
            ['id'=>1,'material_id'=>3756,'name'=>'综合'],
            ['id'=>2,'material_id'=>3767,'name'=>'女装'],
            ['id'=>3,'material_id'=>3758,'name'=>'家居家装'],
            ['id'=>4,'material_id'=>3759,'name'=>'数码家电'],
            ['id'=>5,'material_id'=>3762,'name'=>'鞋包配饰'],
            ['id'=>6,'material_id'=>3763,'name'=>'美妆个护'],
            ['id'=>7,'material_id'=>3764,'name'=>'男装'],
            ['id'=>8,'material_id'=>3765,'name'=>'内衣'],
            ['id'=>9,'material_id'=>3760,'name'=>'母婴'],
            ['id'=>9,'material_id'=>3761,'name'=>'食品'],
            ['id'=>9,'material_id'=>3766,'name'=>'运动户外'],
        ];
        //高佣榜,500款商品,按天更新
        $category1=[
            ['id'=>1,'material_id'=>13366,'name'=>'综合'],
            ['id'=>2,'material_id'=>13367,'name'=>'女装'],
            ['id'=>3,'material_id'=>13368,'name'=>'家居家装'],
            ['id'=>4,'material_id'=>13369,'name'=>'数码家电'],
            ['id'=>5,'material_id'=>13370,'name'=>'鞋包配饰'],
            ['id'=>6,'material_id'=>13371,'name'=>'美妆个护'],
            ['id'=>7,'material_id'=>13372,'name'=>'男装'],
            ['id'=>8,'material_id'=>13373,'name'=>'内衣'],
            ['id'=>9,'material_id'=>13374,'name'=>'母婴'],
            ['id'=>9,'material_id'=>13375,'name'=>'食品'],
            ['id'=>9,'material_id'=>13376,'name'=>'运动户外'],
        ];
        //品牌券,200款商品
        $category2=[
            ['id'=>1,'material_id'=>3786,'name'=>'综合'],
            ['id'=>2,'material_id'=>3788,'name'=>'女装'],
            ['id'=>3,'material_id'=>3792,'name'=>'家居家装'],
            ['id'=>4,'material_id'=>3793,'name'=>'数码家电'],
            ['id'=>5,'material_id'=>3796,'name'=>'鞋包配饰'],
            ['id'=>6,'material_id'=>3794,'name'=>'美妆个护'],
            ['id'=>7,'material_id'=>3790,'name'=>'男装'],
            ['id'=>8,'material_id'=>3787,'name'=>'内衣'],
            ['id'=>9,'material_id'=>3789,'name'=>'母婴'],
            ['id'=>9,'material_id'=>3791,'name'=>'食品'],
            ['id'=>9,'material_id'=>3795,'name'=>'运动户外'],
        ];

        return $this->response->array($category);
    }
}
