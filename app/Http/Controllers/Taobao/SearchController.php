<?php

namespace App\Http\Controllers\Taobao;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

class SearchController extends BaseController
{
    /**
     * 淘宝联盟物料搜索
     * @param Request $request
     * @return false|string
     */
    public function index(Request $request){
        $req = new \TbkDgMaterialOptionalRequest();
        $req->setAdzoneId(env('taobao_adzone_id'));
        $req->setPageSize($request->get('pageSize') ?? 20);
        $req->setPageNo($request->get('page') ?? 1);
        $req->setQ($request->get('keyword'));
        $req->setMaterialId('6707');// 默认2836，个性化算法17004换成6707
        $res = $this->taobao_c()->execute($req);
        if (empty($res->result_list)){
            return json_encode($res);
        }
        return json_encode($res->result_list->map_data);
    }
}
