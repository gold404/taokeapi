<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use  App\Http\Controllers\Taobao\IndexController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {

    $api->get('dataoke',function (){
        $client=new GetTipList();
        $client->setAppKey('60fa8035d77d5');
        $client->setAppSecret('fd4f2170214df9b957f9a33a67798eaf');
        $client->setVersion('v3.0.0');

        $res = $client->setParams(['topic'=>1])->request();
        return $res;
    });

    //今日爆款(综合类目)
    $api->get('recommend',[IndexController::class,'recommend']);
    //商品详情
    $api->get('detail',[IndexController::class,'detail']);
    //相似推荐
    $api->get('similar',[IndexController::class,'similar']);
    //分类
    $api->get('category',[IndexController::class,'category']);
    $api->get('category/list',[IndexController::class,'categoryList']);
    //搜索
    $api->get('search',[\App\Http\Controllers\Taobao\SearchController::class,'index']);

    $api->group(['prefix' => 'auth'],function ($api){
        $api->post('register',[\App\Http\Controllers\Auth\RegisterController::class,'store']);
        $api->post('login',[LoginController::class,'login']);
        //需要登录的路由
        $api->group(['middleware'=> 'api.auth'],function ($api){
            $api->get('refresh',[LoginController::class,'refresh']);
            $api->get('me',[LoginController::class,'me']);
            $api->get('logout',[LoginController::class,'logout']);
        });

    });

    $api->group(['middleware'=> ['api.auth','bindings']],function ($api){
        $api->resource('banners',\App\Http\Controllers\BannerController::class);
        $api->patch('banner/{banner}/sort',[\App\Http\Controllers\BannerController::class,'sort']);
    });


});
