# 淘客返利商城后端api

## 介绍
本项目是一个基于laravel8框架的淘宝客后端项目，对接淘宝联盟、多多进宝、京东联盟等等。也可能会提供一些关于淘客的开放api，欢迎来撩。

## 安装教程

1、下载源码：git clone https://gitee.com/gold404/taokeapi.git

2、使用composer安装依赖：composer install

3、把.env.example文件名重命名为.env，并把其中的淘宝联盟配置信息换成自己的（AppKey和ppSecret）。

4、执行命令（生成laravel密钥）：php artisan key:generate
生成jwt密钥：php artisan jwt:secret


执行完上面操作之后，项目就可以飞快的跑起来啦！

## 项目api接口列表

本项目api接口文档地址：https://www.showdoc.cc/1164305758936225


#### 淘宝联盟

1、今日爆款推荐

2、分类小标题获取。如：综合、女装、男装、数码家电、母婴、食品......

3、商品列表。查询某一分类下的商品

4、商品详情。

5、相似推荐。查询出和当前商品相似的几个商品。

未完待续......

#### 多多进宝

尚未对接，敬请期待！

#### 京东联盟

尚未对接，敬请期待！
